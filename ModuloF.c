#include <stdio.h>
#include <stdlib.h>
#include "ModuloF.h"




void ModuloF()
{
	int opcao=0,op=0,troca=0,comando=0,escolha=0,discobackup=0,backup=0,discoparidade=0,paridade=0;
	int disco0=0,disco1=0,disco2=0,disco3=0,disc0=0,disc1=0,disc2=0,disc3=0, elemento=0,aaux=0;
	do
	{
			printf("\n");
			printf("\nEscolha um sistema RAID 3 ou RAID 4");
			printf("\n(1) RAID 3");
			printf("\n(2) RAID 4");
			printf("\n(3) Sair");
			printf("\nSua escolha:  ");
			scanf("%d",&opcao);
			if(opcao==1)
			{
				printf("\n(1) Dados de 4 bytes");
				printf("\n(2) Dados de 16 bytes");
				printf("\nSua escolha:  ");
				scanf("%d",&op);
				if(op==1)
				{
						printf("\nEscolha um comando para emular o sistema RAID 3");
						printf("\n(1) Inserir um valor no disco");
						printf("\n(2) Emular uma falha no disco de sua escolha");
						printf("\nSua escolha:  ");
						scanf("%d",&escolha);
						if(escolha==1)
						{
								int valor=0;
								printf("\nDigite um valor a ser inserido no disco:  ");
								scanf("%d",&valor);
								if(disco0==0)
								{
									printf("\nConteudo do disco0 antes da escrita:%d",disco0);
									disco0=valor;
									printf("\nValor a ser escrito no disco 0:%d",valor);
									printf("\nConteudo do disco0 apos a escrita:%d",disco0);
								}
								else if(disco0!=0 && disco1==0)
								{
									printf("\nConteudo do disco1 antes da escrita:%d",disco1);
									disco1=valor;
									printf("\nValor a ser escrito no disco 1:%d",valor);
									printf("\nConteudo do disco1 apos a escrita:%d",disco1);
								}
								else if(disco0!=0 && disco1!=0 && disco2==0)
								{
									printf("\nConteudo do disco2 antes da escrita:%d",disco2);
									disco2=valor;
									printf("\nValor a ser escrito no disco 2:%d",valor);
									printf("\nConteudo do disco2 apos a escrita:%d",disco2);
								}
								else if(disco0!=0 && disco1!=0 && disco2!=0 && disco3==0)
								{
									printf("\nConteudo do disco3 antes da escrita:%d",disco3);
									disco3=valor;
									printf("\nValor a ser escrito no disco 3:%d",valor);
									printf("\nConteudo do disco3 apos a escrita:%d",disco3);
								}
								printf("\nConteudo dos disco0:%d disco1:%d disco2:%i disco3:%i (antes da escrita no disco de paridade)",disco0,disco1,disco2,disco3);
								printf("\nConteudo do disco de paridade antes da escrita:%d",paridade);
								paridade=disco0^disco1^disco2^disco3;
								printf("\nConteudo do disco de paridade apos a escrita:%d",paridade);
						}
						if(escolha==2)
						{
							int disk=0;
							printf("\nEscolha um disco para emular uma falha (0)disco0 (1)disco1 (2)disco2 (3)disco3:  ");
							scanf("%d",&disk);
								if(disk==0)
								{
									disco0=0;
									printf("\nConteudo dos disco0:%d (conteudo perdido) disco1:%d disco2:%d disco3:%d disco de paridade:%d (antes de recuperar os dados do disco0)",disco0,disco1,disco2,disco3,paridade);
									disco0=paridade^disco1^disco2^disco3;
									printf("\nConteudo do disco0 após recuperar os dados:%d",disco0);
								}
								if(disk==1)
								{
									disco1=0;
									printf("\nConteudo dos disco0:%d disco1:%d (conteudo perdido) disco2:%d disco3:%d disco de paridade:%d (antes de recuperar os dados do disco1)",disco0,disco1,disco2,disco3,paridade);
									disco1=paridade^disco0^disco2^disco3;
									printf("\nConteudo do disco1 após recuperar os dados:%d",disco1);
								}
								if(disk==2)
								{
									disco2=0;
									printf("\nConteudo dos disco0:%d disco1:%d disco2:%d (conteudo perdido) disco3:%d disco de paridade:%d (antes de recuperar os dados do disco2)",disco0,disco1,disco2,disco3,paridade);
									disco2=paridade^disco0^disco1^disco3;
									printf("\nConteudo do disco2 após recuperar os dados:%d",disco2);
								}
								if(disk==3)
								{
									disco3=0;
									printf("\nConteudo dos disco0:%d disco1:%d disco2:%d disco3: %d (conteudo perdido) disco de paridade:%d (antes de recuperar os dados do disco3)",disco0,disco1,disco2,disco3,paridade);
									disco3=paridade^disco0^disco1^disco2;
									printf("\nConteudo do disco3 após recuperar os dados:%d",disco3);
								}
						}
					}
						if(op==2)
						{
							escolha=0;
							printf("\nEscolha um comando para emular o sistema RAID 3");
							printf("\n(1) Inserir um valor no disco");
							printf("\n(2) Emular uma falha no disco de sua escolha");
							printf("\nSua escolha:  ");
							scanf("%d",&escolha);
							if(escolha==1)
							{
								discoparidade=RAID3_valordodisco16bytes();
							}
							if(escolha==2)
							{
								RAID3_emularfalha16bytes(discoparidade);
							}
						}
				}
			if(opcao==2)
			{
				comando=0;
				printf("\n(1) Dados de 4 bytes");
				printf("\n(2) Dados de 16 bytes");
				printf("\nSua escolha:  ");
				scanf("%d",&troca);
				if(troca==1)
				{
					printf("\nEscolha um comando para emular os sistema RAID 4");
					printf("\n(1) Inserir um valor no disco");
					printf("\n(2) Emular uma falha no disco de sua escolha");
					printf("\nSua escolha:  ");
					scanf("%d",&comando);
					if(comando==1)
	    		{
						printf("\nDigite um valor a ser inserido no disco:  ");
						scanf("%d",&elemento);
						if(disc0==0)
						{
							printf("\nConteudo do disco0 antes da escrita:%d",disc0);
							aaux=disc0^elemento;
							backup=backup^aaux;
							disc0=elemento;
							printf("\nValor a ser escrito no disco 0:%d",elemento);
							printf("\nConteudo do disco0 apos a escrita:%d",disc0);
							printf("\nConteudo do disco0:%d (antes da escrita no disco de paridade)",disc0);
							printf("\nConteudo do disco de paridade antes da escrita:%d",backup);
							aaux=disc0^elemento;
							backup=backup^aaux;
							printf("\nConteudo do disco de paridade apos a escrita:%d",backup);
						}
						else if(disc0!=0 && disc1==0)
						{
							printf("\nConteudo do disco1 antes da escrita:%d",disc1);
							aaux=disc1^elemento;
							backup=backup^aaux;
							disc1=elemento;
							printf("\nValor a ser escrito no disco 1:%d",elemento);
							printf("\nConteudo do disco1 apos a escrita:%d",disc1);
							printf("\nConteudo do disco1:%d (antes da escrita no disco de paridade)",disc1);
							printf("\nConteudo do disco de paridade antes da escrita:%d",backup);
							aaux=disc1^elemento;
							backup=backup^aaux;
							printf("\nConteudo do disco de paridade apos a escrita:%d",backup);
						}
						else if(disc0!=0 && disc1!=0 && disc2==0)
						{
							printf("\nConteudo do disco2 antes da escrita:%d",disc2);
							aaux=disc2^elemento;
							backup=backup^aaux;
							disc2=elemento;
							printf("\nValor a ser escrito no disco 2:%d",elemento);
							printf("\nConteudo do disco2 apos a escrita:%d",disc2);
							printf("\nConteudo do disco2:%d (antes da escrita no disco de paridade)",disc2);
							printf("\nConteudo do disco de paridade antes da escrita:%d",backup);
							aaux=disc2^elemento;
							backup=backup^aaux;
							printf("\nConteudo do disco de paridade apos a escrita:%d",backup);
						}
						else if(disc0!=0 && disc1!=0 && disc2!=0 && disc3==0)
						{
							printf("\nConteudo do disco3 antes da escrita:%d",disc3);
							aaux=disc3^elemento;
							backup=backup^aaux;
							disc3=elemento;
							printf("\nValor a ser escrito no disco 3:%d",elemento);
							printf("\nConteudo do disco3 apos a escrita:%d",disc3);
							printf("\nConteudo do disco3:%d (antes da escrita no disco de paridade)",disc3);
							printf("\nConteudo do disco de paridade antes da escrita:%d",backup);
							printf("\nConteudo do disco de paridade apos a escrita:%d",backup);
						}
					}
					if(comando==2)
					{
						int disc=0;
						printf("\nEscolha um disco para emular uma falha (0)disco0 (1)disco1 (2)disco2 (3)disco3:  ");
						scanf("%d",&disc);
						if(disc==0)
						{
							disc0=0;
							printf("\nConteudo dos disco0:%d (conteudo perdido) disco1:%d disco2:%d disco3:%d disco de paridade:%d (antes de recuperar os dados do disco0)",disc0,disc1,disc2,disc3,backup);
							disc0=backup^disc1^disc2^disc3;
							printf("\nConteudo do disco0 após recuperar os dados:%d",disc0);
						}
						if(disc==1)
						{
							disc1=0;
							printf("\nConteudo dos disco0:%d disco1:%d (conteudo perdido) disco2:%d disco3:%d disco de paridade:%d (antes de recuperar os dados do disco1)",disc0,disc1,disc2,disc3,backup);
							disc1=backup^disc0^disc2^disc3;
							printf("\nConteudo do disco1 após recuperar os dados:%d",disc1);
						}
						if(disc==2)
						{
							disc2=0;
							printf("\nConteudo dos disco0:%d disco1:%d disco2:%d (conteudo perdido) disco3:%d disco de paridade:%d (antes de recuperar os dados do disco2)",disc0,disc1,disc2,disc3,backup);
							disc2=backup^disc0^disc1^disc3;
							printf("\nConteudo do disco2 após recuperar os dados:%d",disc2);
						}
						if(disc==3)
						{
							disc3=0;
							printf("\nConteudo dos disco0:%d disco1:%d disco2:%d disco3:%d (conteudo perdido) disco de paridade:%d (antes de recuperar os dados do disco3)",disc0,disc1,disc2,disc3,backup);
							disc3=backup^disc0^disc1^disc2;
							printf("\nConteudo do disco3 após recuperar os dados:%d",disc3);
						}
					}
				}
			if(troca==2)
			{
				comando=0;
				printf("\nEscolha um comando para emular os sistema RAID 4");
				printf("\n(1) Inserir um valor no disco");
				printf("\n(2) Emular uma falha no disco de sua escolha");
				printf("\nSua escolha:  ");
				scanf("%d",&comando);
				if(comando==1)
				{
						discobackup=RAID4_valordodisco16bytes();
				}
				if(comando==2)
				{
						RAID4_emularfalha16bytes(discobackup);
				}
			}
		}
		if(opcao==3)
		{

		}
	}while(opcao!=3);

}

int RAID3_valordodisco16bytes()
{
	int discoparidade=0,element=0;
	printf("\nDigite um valor a ser inserido no disco:  ");
	scanf("%d",&element);
	printf("\nConteudo do disco0 antes da escrita:%d",disco16bytes3[0]);
	disco16bytes3[0]=(int)element;
	printf("\nValor a ser escrito no disco 0:%d",element);
	printf("\nConteudo do disco0 apos a escrita:%d",disco16bytes3[0]);
	printf("\nConteudo do disco1 antes da escrita:%d",disco16bytes3[1]);
	disco16bytes3[1]=(int)element;
	printf("\nValor a ser escrito no disco 1:%d",element);
	printf("\nConteudo do disco1 apos a escrita:%d",disco16bytes3[1]);
	printf("\nConteudo do disco2 antes da escrita:%d",disco16bytes3[2]);
	disco16bytes3[2]=(int)element;
	printf("\nValor a ser escrito no disco 2:%d",element);
	printf("\nConteudo do disco2 apos a escrita:%d",disco16bytes3[2]);
	printf("\nConteudo do disco3 antes da escrita:%d",disco16bytes3[3]);
	disco16bytes3[3]=(int)element;
	printf("\nValor a ser escrito no disco 3:%d",element);
	printf("\nConteudo do disco3 apos a escrita:%d",disco16bytes3[3]);
	printf("\nConteudo dos disco0:%d disco1:%d disco2:%i disco3:%i (antes da escrita no disco de paridade)",disco16bytes3[0],disco16bytes3[1],disco16bytes3[2],disco16bytes3[3]);
	printf("\nConteudo do disco de paridade antes da escrita:%d",discoparidade);
	discoparidade=disco16bytes3[0]^disco16bytes3[1]^disco16bytes3[2]^disco16bytes3[3];
	printf("\nConteudo do disco de paridade apos a escrita:%d",discoparidade);
	return discoparidade;
}

void RAID3_emularfalha16bytes(int discoparidade)
{
	int disk=0;
	printf("\nEscolha um disco para emular uma falha (0)disco0 (1)disco1 (2)disco2 (3)disco3:  ");
	scanf("%d",&disk);
	if(disk==0)
	{
		disco16bytes3[0]=0;
		printf("\nConteudo dos disco0:%d (conteudo perdido) disco1:%d disco2:%d disco3:%d disco de paridade:%d (antes de recuperar os dados do disco0)",disco16bytes3[0],disco16bytes3[1],disco16bytes3[2],disco16bytes3[3],discoparidade);
		disco16bytes3[0]=(int)discoparidade^disco16bytes3[1]^disco16bytes3[2]^disco16bytes3[3];
		printf("\nConteudo do disco0 após recuperar os dados:%d",disco16bytes3[0]);
	}
	if(disk==1)
	{
		disco16bytes3[1]=0;
		printf("\nConteudo dos disco0:%d disco1:%d (conteudo perdido) disco2:%d disco3:%d disco de paridade:%d (antes de recuperar os dados do disco1)",disco16bytes3[0],disco16bytes3[1],disco16bytes3[2],disco16bytes3[3],discoparidade);
		disco16bytes3[1]=(int)discoparidade^disco16bytes3[0]^disco16bytes3[2]^disco16bytes3[3];
		printf("\nConteudo do disco1 após recuperar os dados:%d",disco16bytes3[1]);
	}
	if(disk==2)
	{
		disco16bytes3[2]=0;
		printf("\nConteudo dos disco0:%d disco1:%d disco2:%d (conteudo perdido) disco3:%d disco de paridade:%d (antes de recuperar os dados do disco2)",disco16bytes3[0],disco16bytes3[1],disco16bytes3[2],disco16bytes3[3],discoparidade);
		disco16bytes3[2]=(int)discoparidade^disco16bytes3[0]^disco16bytes3[1]^disco16bytes3[3];
		printf("\nConteudo do disco2 após recuperar os dados:%d",disco16bytes3[2]);
	}
	if(disk==3)
	{
		disco16bytes3[3]=0;
		printf("\nConteudo do disco0:%d disco1:%d disco2:%d disco3:%d (conteudo perdido) disco de paridade:%d (antes de recuperar os dados do disco3)",disco16bytes3[0],disco16bytes3[1],disco16bytes3[2],disco16bytes3[3],discoparidade);
		disco16bytes3[3]=(int)discoparidade^disco16bytes3[0]^disco16bytes3[1]^disco16bytes3[2];
		printf("\nConteudo do disco3 após recuperar os dados:%d",disco16bytes3[3]);
	}
}

int RAID4_valordodisco16bytes()
{
	int discobackup=0,element=0,aaux=0;
	printf("\nDigite um valor a ser inserido no disco:  ");
scanf("%d",&element);
if(disco16bytes4[0]==0)
{
	printf("\nConteudo do disco0 antes da escrita:%d",disco16bytes4[0]);
	aaux=disco16bytes4[0]^element;
	discobackup=discobackup^aaux;
	disco16bytes4[0]=element;
	printf("\nValor a ser escrito no disco 0:%d",element);
	printf("\nConteudo do disco0 apos a escrita:%d",disco16bytes4[0]);
	printf("\nConteudo do disco0:%d (antes da escrita no disco de paridade)",disco16bytes4[0]);
	printf("\nConteudo do disco de paridade antes da escrita:%d",discobackup);
	aaux=disco16bytes4[0]^element;
	discobackup=discobackup^aaux;
	printf("\nConteudo do disco de paridade apos a escrita:%d",discobackup);
}
else if(disco16bytes4[0]!=0 && disco16bytes4[1]==0)
{
	printf("\nConteudo do disco1 antes da escrita:%d",disco16bytes4[1]);
	aaux=disco16bytes4[1]^element;
	discobackup=discobackup^aaux;
	disco16bytes4[1]=element;
	printf("\nValor a ser escrito no disco 1:%d",element);
	printf("\nConteudo do disco1 apos a escrita:%d",disco16bytes4[1]);
	printf("\nConteudo do disco1:%d (antes da escrita no disco de paridade)",disco16bytes4[1]);
	printf("\nConteudo do disco de paridade antes da escrita:%d",discobackup);
	aaux=disco16bytes4[1]^element;
	discobackup=discobackup^aaux;
	printf("\nConteudo do disco de paridade apos a escrita:%d",discobackup);
}
else if(disco16bytes4[0]!=0 && disco16bytes4[1]!=0 && disco16bytes4[2]==0)
{
	printf("\nConteudo do disco2 antes da escrita:%d",disco16bytes4[2]);
	aaux=disco16bytes4[2]^element;
	discobackup=discobackup^aaux;
	disco16bytes4[2]=element;
	printf("\nValor a ser escrito no disco 2:%d",element);
	printf("\nConteudo do disco2 apos a escrita:%d",disco16bytes4[2]);
	printf("\nConteudo do disco2:%d (antes da escrita no disco de paridade)",disco16bytes4[2]);
	printf("\nConteudo do disco de paridade antes da escrita:%d",discobackup);
	aaux=disco16bytes4[2]^element;
	discobackup=discobackup^aaux;
	printf("\nConteudo do disco de paridade apos a escrita:%d",discobackup);
}
else if(disco16bytes4[0]!=0 && disco16bytes4[1]!=0 && disco16bytes4[2]!=0 && disco16bytes4[3]==0)
{
	printf("\nConteudo do disco3 antes da escrita:%d",disco16bytes4[3]);
	aaux=disco16bytes4[3]^element;
	discobackup=discobackup^aaux;
	disco16bytes4[3]=element;
	printf("\nValor a ser escrito no disco 3:%d",element);
	printf("\nConteudo do disco3 apos a escrita:%d",disco16bytes4[3]);
	printf("\nConteudo do disco3:%d (antes da escrita no disco de paridade)",disco16bytes4[3]);
	printf("\nConteudo do disco de paridade antes da escrita:%d",discobackup);
	aaux=disco16bytes4[3]^element;
	discobackup=discobackup^aaux;
	printf("\nConteudo do disco de paridade apos a escrita:%d",discobackup);
}
return discobackup;
}

void RAID4_emularfalha16bytes(int discobackup)
{
	int disc=0;
	printf("\nEscolha um disco para emular uma falha (0)disco0 (1)disco1 (2)disco2 (3)disco3:  ");
	scanf("%d",&disc);
	if(disc==0)
	{
		disco16bytes4[0]=0;
		printf("\nConteudo dos disco0:%d (conteudo perdido) disco1:%d disco2:%d disco3:%d disco de paridade:%d (antes de recuperar os dados do disco0)",disco16bytes4[0],disco16bytes4[1],disco16bytes4[2],disco16bytes4[3],discobackup);
		disco16bytes4[0]=discobackup^disco16bytes4[1]^disco16bytes4[2]^disco16bytes4[3];
		printf("\nConteudo do disco0 após recuperar os dados:%d",disco16bytes4[0]);
	}
	if(disc==1)
	{
		disco16bytes4[1]=0;
		printf("\nConteudo do disco0:%d disco1:%d (conteudo perdido) disco2:%d disco3:%d disco de paridade:%d (antes de recuperar os dados do disco1)",disco16bytes4[0],disco16bytes4[1],disco16bytes4[2],disco16bytes4[3],discobackup);
		disco16bytes4[1]=discobackup^disco16bytes4[0]^disco16bytes4[2]^disco16bytes4[3];
		printf("\nConteudo do disco1 após recuperar os dados:%d",disco16bytes4[1]);
	}
	if(disc==2)
	{
		disco16bytes4[2]=0;
		printf("\nConteudo do disco0:%d disco1:%d disco2:%d (conteudo perdido) disco3:%d disco de paridade:%d (antes de recuperar os dados do disco2)",disco16bytes4[0],disco16bytes4[1],disco16bytes4[2],disco16bytes4[3],discobackup);
		disco16bytes4[2]=discobackup^disco16bytes4[0]^disco16bytes4[1]^disco16bytes4[3];
		printf("\nConteudo do disco2 após recuperar os dados:%d",disco16bytes4[2]);
	}
	if(disc==3)
	{
		disco16bytes4[3]=0;
		printf("\nConteudo do disco0:%d disco1:%d disco2:%d disco3:%d (conteudo perdido) disco de paridade:%d (antes de recuperar os dados do disco3)",disco16bytes4[0],disco16bytes4[1],disco16bytes4[2],disco16bytes4[3],discobackup);
		disco16bytes4[3]=discobackup^disco16bytes4[0]^disco16bytes4[1]^disco16bytes4[2];
		printf("\nConteudo do disco3 após recuperar os dados:%d",disco16bytes4[3]);
	}
}
