
struct listaf{
	int info;
	struct listaf*prox;
};typedef struct listaf ListaF;

struct fila {
	ListaF*ini;
	ListaF*fim;
};typedef struct fila Fila;

Fila* fila_cria();
void fila_insere(Fila*f,int v);
int fila_retira(Fila*f);
void fila_libera(Fila*f);
void imprimir_fila(Fila*f);

