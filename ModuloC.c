
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ModuloC.h"





void ModuloC(){
float frequencia;
float LatMp,LatDados2,LatInst2;
float cpi;
float LoadStore;
float TDados1,TDados2,TInst1,TInst2;
float CalcInst1;
float CalcInst2;
float CalcDados1;
float CalcDados2;
float StallM1,StallM2;
float ncpi1,ncpi2;
int op;


do{
printf("\n-------------------------------------------------------------------\n");
printf("----------------------MODULO C--CALCULO DO CPI-----------------------\n");
printf("---------------------------------------------------------------------\n");
printf("|1| Digite Frequencia de operacao do processador (em GHz): ");
scanf("%f",&frequencia);
printf("|2| Digite Latencia para acesso a bloco na memoria principal (em ns): ");
scanf("%f",&LatMp);
printf("|3| Digite Latencia para acesso a cache de dados nivel 2 (em ns): ");
scanf("%f",&LatDados2);
printf("|4| Digite Latencia para acesso a cache de instrucoes nivel 2(em ns): ");
scanf("%f",&LatInst2);
printf("|5| Digite o CPI basico do processador (quando executa sem falhas na cache): ");
scanf("%f",&cpi);
printf("|6| Digite Taxa de falhas na cache nivel 1 de dados: ");
scanf("%f",&TDados1);
printf("|7| Digite Taxa de falhas na cache nivel 1 de instrucoes: ");
scanf("%f",&TInst1);
printf("|8| Digite Taxa de falhas na cache nivel 2 de dados: ");
scanf("%f",&TDados2);
printf("|9| Digite Taxa de falhas na cache nivel 2 de instrucoes: ");
scanf("%f",&TInst2);
printf("|10| Digite Percentual de instruções do tipo Load/Store: ");
scanf("%f",&LoadStore);
printf("\n\n");
printf("\e[H\e[2J");
printf("\n-------------------------------------------------------------------\n");
printf("----------------------MODULO C--CALCULO DO CPI---------------------\n");
printf("-------------------------------------------------------------------\n\n");
printf("-----------------------------------------\n");
printf("------Dados Fornecidos pelo Usuario------\n");
printf("-----------------------------------------\n");
printf("Frequencia(em GHz): %.0f\n",frequencia);
printf("Tempo de Acesso a Ram (em ns): %.0f\n",LatMp);
printf("Tempo de Acesso a L2 de dados (em ns): %.0f\n",LatDados2);;
printf("Tempo de Acesso a L2 de instrucoes (em ns): %.0f\n",LatInst2);
printf("Cpi Processador(sem falhas da memoria): %.0f\n",cpi);
printf("Taxa de falhas de dados L1: %.0f\n",TDados1);
printf("Taxa de falhas de Instrucoes L1: %.0f\n",TInst1);
printf("Taxa de falhas de dados L2: %.0f\n",TDados2);
printf("Taxa de falhas de Instrucoes L2: %.0f\n",TInst2);
printf("Percentual de instrucoes do tipo Load/Store: %.0f\n",LoadStore);
printf("\n-----------------------------------------------------------------\n");
printf("--------------CPI SEM USO DAS CACHES DE NIVEL DOIS---------------\n");
printf("-----------------------------------------------------------------");
CalcInst1=CalcFalhasInst1(TInst1,frequencia,LatMp);
CalcDados1=CalcFalhasDados1(TDados1,frequencia,LatMp,LoadStore);
StallM1=StallMemory1(CalcInst1,CalcDados1);
ncpi1=NovoCpi1(cpi,StallM1);
PerdaDesempenho1(ncpi1,cpi);
printf("\n-----------------------------------------------------------------\n");
printf("---------------CPI COM USO DAS CACHES DE NIVEL 2-----------------\n");
printf("-----------------------------------------------------------------\n");
CalcInst2=CalcFalhasInst2(TInst1,frequencia,LatInst2,LatMp,TInst2);
CalcDados2=CalcFalhasDados2(TDados1,TDados2,frequencia,LatDados2,LatMp,LoadStore);
StallM2=StallMemory2(CalcInst2,CalcDados2);
ncpi2=NovoCpi1(cpi,StallM2);
PerdaDesempenho1(ncpi2,cpi);
printf("\n\n");
printf("|1|Calcular Novamente.\n");
printf("|0|Voltar.\n");
printf("Opcao: ");
scanf("%d",&op);


}while(op!=0);



}

float CalcFalhasInst1(float TInst1, float frequencia,float LatMp){

float resultado;
float nciclos;
float converte;
converte=TInst1/100;
nciclos=(1/frequencia);
float penalidade;
penalidade=(LatMp/nciclos);
resultado=converte*penalidade;
printf("\n\nCiclos de Falhas de Instrucao L1: %.2f\n",resultado);
return resultado;
}

float CalcFalhasDados1(float IntDados,float frequencia,float LatMp,float LoadStore){

float resultado;
float nciclos;
float converte;
float converte1;
converte=IntDados/100;
converte1=LoadStore/100;
nciclos=(1/frequencia);
float penalidade;
penalidade=(LatMp/nciclos);
resultado=converte*penalidade*converte1;
printf("Ciclos de Falha de Dados L1: %.2f\n",resultado);
return resultado;
}

float StallMemory1(float CalcInst1,float CalcDados1){

float resultado=CalcInst1+CalcDados1;
printf("Total de ciclos de espera pela Memoria L1: %.2f\n",resultado);
return resultado;

}

float NovoCpi1(float cpi1,float StallM1){

float Novocpi1;
Novocpi1=cpi1+StallM1;


printf("Novo Cpi: %.2f\n",Novocpi1);
return Novocpi1;
}

void PerdaDesempenho1(float ncpi1,float cpi){

float resultado;
resultado=ncpi1/cpi;
//printf("Perda Desempenho devido a falhas: %.2f\n",resultado);
}

float CalcFalhasInst2(float TInst1,float frequencia,float LatInst2,float LatMp,float TInst2){
//falhas em l1 //acessos a L2
float FalhasEmL1;
//Falhas em l2 //accessos a ram
float FalhasEmL2;
float resultado;
float nciclos;
float converte;
float converte2;
converte=TInst1/100;
converte2=TInst2/100;
nciclos=(1/frequencia);
float penalidade;
penalidade=LatInst2/nciclos;
FalhasEmL1=converte*penalidade;
printf("Ciclos de Falhas de instrucao a L1: %.1f\n",FalhasEmL1);
    float penalidadeL2;
    penalidadeL2=(LatMp/nciclos);
    FalhasEmL2=converte*penalidadeL2*converte2;
    printf("Ciclos de falhas de instrucao a L2: %.1f\n",FalhasEmL2);
    resultado=FalhasEmL1+FalhasEmL2;
    printf("Ciclos para tratar falhas do caminho de intrucoes: %.2f\n\n",resultado);

return resultado;
}

float CalcFalhasDados2(float TDados1,float TDados2,float frequencia,float LatDados2,float LatMp,float loadStore){
//falhas em l1 //acessos a L2
float FalhasEmL1;
//Falhas em l2 //accessos a ram
float FalhasEmL2;
float resultado;
float nciclos;
float TaxaDados1;
float TaxaDados2;
TaxaDados1=TDados1/100;
TaxaDados2=TDados2/100;
float LoadStore;
LoadStore=loadStore/100;
nciclos=(1/frequencia);
float penalidadeL1;
penalidadeL1=(LatDados2/nciclos);
FalhasEmL1=TaxaDados1*penalidadeL1*LoadStore;
printf("Ciclos de Falhas de Dados a L1: %.2f\n",FalhasEmL1);
    float penalidadeL2;
    penalidadeL2=(LatMp/nciclos);
    FalhasEmL2=TaxaDados1*penalidadeL2*TaxaDados2*LoadStore;
    printf("Ciclos de falhas de Dados a L2: %.2f\n",FalhasEmL2);

    resultado=FalhasEmL1+FalhasEmL2;
    printf("Ciclos para tratar falhas do caminho de Dados: %.2f\n",resultado);
    return resultado;
}

float StallMemory2(float CalcInst2,float CalcDados2){
float resultado=CalcInst2+CalcDados2;
printf("\nTotal de ciclos de espera pela Memoria : %.2f\n",resultado);
return resultado;


}

float NovoCpi2(float cpi2,float StallM2){

float Novocpi2;
Novocpi2=cpi2+StallM2;

printf("Novo CPI: %f\n",Novocpi2);
return Novocpi2;
}

void PerdaDesempenho2(float ncpi2,float cpi){

float resultado;
resultado=(ncpi2/cpi);
printf("Perda Desempenho devido a falhas: %f\n\n",resultado);
}


