
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "ModuloG.h"




void ModuloG(){


int LatenciaAcessoMemoria;
int LatenciaPasso1,LatenciaPasso234,LatenciaPasso567;
int latenciatotal;

float larguraBanda;
printf("Digite Latencia Passo1(ns):");
scanf("%d",&LatenciaPasso1);
printf("Digite Latencia Passo2,Passo3,Passo4(ns): ");
scanf("%d",&LatenciaPasso234);
printf("Digite Latencia Passo5,Passo6,Passo7(ns):");
scanf("%d",&LatenciaPasso567);
printf("Latência de acesso à memória para leitura da palavra solicitada(ns): ");
scanf("%d",&LatenciaAcessoMemoria);

printf("\n");

Memoria();
    if((LatenciaPasso234*3)<LatenciaAcessoMemoria){
        latenciatotal=(LatenciaPasso1)+(LatenciaAcessoMemoria)+((LatenciaPasso567*3));
    }
    if((LatenciaPasso234*3)>LatenciaAcessoMemoria){
    latenciatotal=(LatenciaPasso1)+(LatenciaPasso234*3)+(LatenciaPasso567*3);
    }

printf("Tempo total da leitura solicitada a memoria: %d nanosegundos\n",latenciatotal);
float latencia=(float)latenciatotal;
larguraBanda=4/(latencia/1000);
printf("Largura de banda do barramento Assincrono: %.2f MB/s\n",larguraBanda);




}

void ReqLeit(bool i){
    if(i==true)
        reqleit=true;
    if(i==false)
        reqleit=false;
}

void Ack(bool i){
    if(i==true)
        ack=true;
    if(i==false)
        ack=false;

}

void DadoPrt(bool i){
    if(i==true)
        dadoprt=true;
    if(i==false)
        dadoprt=false;
}

void Memoria(){
    if(PassoMem==0){
        if(reqleit==true){//Memoria enxerga Reqleitura ativa
         MemoriaEnderecos=&LinhaDados;//le endereco
         printf("Endereco lido da linha de Dados: %p\n",MemoriaEnderecos);//le enĿdereco
         Ack(true);//ativa ack
        }
    printf("Passo1 do Protocolo Completado\n\n");
    PassoMem++;
    Dispositivo();//Executa parte dois
    }
    if(PassoMem==1){
        if(reqleit==false)//memoria enxerga reqleitura desativado
        Ack(false);//desativa ack
        PassoMem++;
        printf("Passo3 do Protocolo Completado\n\n");
    }
    if(PassoMem==2){
        LinhaDados=*MemoriaEnderecos;//Memoria disponibila dados no barramento
        printf("Dado colocado na linha de Dados do Barramento: %d\n",LinhaDados);
        DadoPrt(true);//ativa Dadoptr
        PassoMem++;
        printf("Passo4 do Protocolo Completado\n\n");
        Dispositivo();//executa passo cinco
    }
    if(PassoMem==3){
        if(ack==true)//Memoria enxerga ack ativo
        DadoPrt(false);//desativa  Dadosptr
        printf("Passo6 do Protocolo Completado\n\n");
        PassoMem++;
        Dispositivo();//executa passo 7
    }

}


void Dispositivo(){

    //FUNCAO DISPOSITIVO
    if(PassoDisp==0){
        if(ack==true)//E/S enxerga acl ativo
        ReqLeit(false);//desativa reqleitura
        printf("Passo2 do Protocolo Completado\n\n");
        PassoDisp++;
        Memoria();//executa parte 3
    }

    //FUNCAO DISPOSITIVO
    if(PassoDisp==1){
        if(dadoprt==true){//Disp. enxerga dadoptr ativo
        dispositivo=LinhaDados;//le dados do barramento
        printf("Dispositivo le Dados do Barramento: %d\n",dispositivo);
        Ack(true);}//ativa ack
        PassoDisp++;
        printf("Passo5 do Protocolo Completado\n\n");
        Memoria();//executa passo6
    }

    //FUNCAO DISPOSITIVO
    if(PassoDisp==2){
        if(dadoprt==false){//Disp enxerga dadoptr falso
        Ack(false);//desativa ack
PassoDisp++;
    printf("Passo7 do Protocolo Completado\n\n");

    }

    }
}

