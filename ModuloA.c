//NOMES
//RODRIGO SEGUI
//GABRIEL VELASCO

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ModuloA.h"



void ModuloA()
{
int capacidade_cache;
int capacidade;
int largura_palavra;
int numero_conjunto;
int tamanho_index;
int tamanhotag;
int offset;
float over;
int h;
do{


printf("-------------------------------------------------------------------\n");
printf("---------------MODULO A--CALCULO TAMANHO TOTAL DA CACHE------------\n");
printf("-------------------------------------------------------------------\n");
printf("\nCapacidade (em KB) da cache para armazenamento de informacoes do usuario (dados ou instrucoes): ");
scanf("%d",&capacidade);
capacidade_cache=capacidade*1024;
printf("Largura (em bytes) da palavra (tanto para dados como enderecos): ");
scanf("%d",&largura_palavra);

printf("\n-----------------------------------------\n");
printf("------Dados Fornecidos pelo Usuario------\n");
printf("-----------------------------------------\n");
printf("Capacidade (em KB) da cache: %d\n",capacidade);
printf("Largura de palavra(em Bytes): %d\n",largura_palavra);
printf("\n");
printf("-----------------------------------------\n");
printf("-----------RESULTADOS--------------------\n");
printf("-----------------------------------------\n");
numero_conjunto=numero_conjuntos(capacidade_cache,largura_palavra);
tamanho_index=campodeindice(numero_conjunto);
offset=tamanho_offeset_byte(largura_palavra);
tamanhotag=tag(offset,largura_palavra,tamanho_index);
over=overhead(numero_conjunto,tamanhotag);
totalidade(over,capacidade_cache);
printf("\n\nEscolha opcao abaixo: \n");
printf("|0| Voltar.\n");
printf("|1| Repetir.\n");
printf("Opcao: ");
scanf("%d",&h);
}while(h!=0);


}

int tamanho_offeset_byte(int largura_palavra)
{
	int x;
	x=(int)(log10(largura_palavra)/log10(2));

	printf("Offset de Byte: %d\n",x);
	return x;
}


int numero_conjuntos(int capacidade_cache, int largura_palavra)
{
    int numero_conjuntos;

	numero_conjuntos=capacidade_cache/largura_palavra;
	printf("Numero de conjuntos da cache: %d\n",numero_conjuntos);
	return numero_conjuntos;
}

int campodeindice(int numero_conjunto)
{
	int tamanho_index=0;
	tamanho_index=(int)(log10(numero_conjunto)/log10(2));
	printf("Tamanho do campo de indice (bits): %d\n",tamanho_index);
	return tamanho_index;
}

int tag(int offset,int largura_palavra, int tamanho_indice)
{

    int y;
    int x;
    y=largura_palavra*8;
    x=y-offset-tamanho_indice;
	printf("Tamanho da TAG (bits): %d\n",x);
	return x;
}

float overhead(int numero_conjunto,int tamanhotag)
{

	int overhead_bits;
	int overhead_bytes;
	float overhead_kbytes;
	overhead_bits=numero_conjunto*(1+tamanhotag);
	//printf("Overhead da cache (bits): %d\n",overhead_bits);
	overhead_bytes=overhead_bits/8; //para converter em bytes
	//printf("Overhead da cache (bytes): %d\n",overhead_bytes);
	overhead_kbytes=(float)overhead_bytes/1024; //para converter em KB;
	printf("Overhead da cache (Kbytes): %.2f\n",overhead_kbytes);
	return overhead_kbytes;
}

void totalidade(float Overhead,int capacidade_cache)
{
	int z;
	float y;
	int v;
	v=capacidade_cache/1024;
	y=Overhead+v;
	printf("Tamanho total da cache (Kbytes): %.2f\n",y);

}

