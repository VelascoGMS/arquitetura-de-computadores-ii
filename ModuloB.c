
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ModuloB.h"



void ModuloB()
{


int op1,op;


int over;
int h;
int b;

do
{
printf("\n-------------------------------------------------------------------\n");
printf("----------------------MODULO B-------------------------------------\n");
printf("-------------------------------------------------------------------\n");
printf("\nEscolha opcao abaixo: \n");
	printf("|1| Calcular capacidade total da cache\n");
	printf("|2| Calcular o tamanho bloco de memoria do usuario\n");
	printf("|0| Voltar.\n");
	printf("opcao: ");
	scanf("%d",&op1);
	printf("\n");
		switch(op1)
    {
    case 1:
            Parte1();
            break;

    case 2:
            Parte2();
            break;
            }
printf("\n\n");
printf("|1|Calcular Novamente.\n");
printf("|0|Voltar.\n");
printf("Opcao: ");
scanf("%d",&op);


}while(op!=0);


}

int offsetbyte(int largura_palavra)
{
	int x;
	x=(int)(log10(largura_palavra)/log10(2));
	printf("Offeset Byte: %d\n",x);
	return x;
}
int offsetpalavra(int tamanho_bloco)
{
	int x;
	x=(int)(log10(tamanho_bloco)/log10(2));
	printf("Offset Palavra: %d\n",x);
	return x;
}

int numerconjuntos(int capacidade_cache, int largura_palavra,int tamanho_bloco)
{
    int numero_conjuntos;
	numero_conjuntos=capacidade_cache/(largura_palavra*tamanho_bloco);
	printf("Numero de conjuntos da cache: %d\n",numero_conjuntos);
	return numero_conjuntos;
}

int campindice(int numero_conjunto)
{
	int tamanho_index=0;
	tamanho_index=(int)(log10(numero_conjunto)/log10(2));
	printf("Tamanho do campo de indice (bits): %d\n",tamanho_index);
	return tamanho_index;
}

int taga(int offsetbyte,int offset_palavra,int largura_palavra, int tamanho_indice)
{

    int y;
    int x;
    y=largura_palavra*8;
    x=y-offsetbyte-offset_palavra-tamanho_indice;
	printf("Tamanho da TAG (bits): %d\n",x);
	return x;
}

float  overhead1(int numero_conjunto,int tamanhotag)
{

	int y;
	int x;
	float z;
	y=numero_conjunto*(1+tamanhotag);
	x=y/8;
	z=(float)x/1024;
	return z;
}

void t(float over,int capacidade_cache)
{
	float y;
	int z;
	int v;
	v=capacidade_cache/1024;
	y=(float)over+v;
	printf("Totalidade (Kbytes): %.2f\n",y);
	z=y*1024;
}



int nconjuntos(int nbits){
int v;
v=pow(2,nbits);
printf("\nNumero conjuntos (bits): %d",v);
return v;

}
int ntags(int overhead,int nconjunto){
int x,y,z;
y=overhead*8;
x=(y-nconjunto)/nconjunto;
printf("\nTamanho tag (bits): %d",x);
return x;
}
void tamanhobloco(int larguraendereco,int nbits,int ntag){
int x,z,y;
y=larguraendereco*8;
x=y-nbits-ntag;
z=pow(2,x);
printf("\nTamanho do bloco do Usuario: %d\n",z);



}

void Parte1(){

int capacidade;
int largura_palavra;
int tamanho_bloco;
int capacidade_cache;
int numero_conjunto;
int tamanho_index;
int tamanhotag;
float over;

int offset;
int offsetword;
printf("\nCapacidade (em KB) da cache para armazenamento de informacoes do usuario (dados ou instrucoes): ");
scanf("%d",&capacidade);
capacidade_cache=capacidade*1024;
printf("\nLargura (em bytes) da palavra (tanto para dados como enderecos): ");
scanf("%d",&largura_palavra);
printf("\nTamanho do bloco de memoria (em palavras): ");
scanf("%d",&tamanho_bloco);
printf("\n-------------------------------------------------------------------\n");
printf("-------------MODULO B-PARTE 1 CAPACIDADE TOTAL DA CACHE------------\n");
printf("-------------------------------------------------------------------\n\n");
printf("-----------------------------------------\n");
printf("------Dados Fornecidos pelo Usuario------\n");
printf("-----------------------------------------\n");
printf("Capacidade (em KB) da cache: %d\n",capacidade);
printf("Largura de palavra (em Bytes): %d\n",largura_palavra);
printf("Tamanho do Bloco (em palavras): %d\n",tamanho_bloco);
printf("\n");
printf("-----------------------------------------\n");
printf("-----------RESULTADOS--------------------\n");
printf("-----------------------------------------\n");

numero_conjunto=numerconjuntos(capacidade_cache,largura_palavra,tamanho_bloco);
tamanho_index=campindice(numero_conjunto);
offset=offsetbyte(largura_palavra);
offsetword=offsetpalavra(tamanho_bloco);
tamanhotag=taga(offset,offsetword,largura_palavra,tamanho_index);
over=overhead1(numero_conjunto,tamanhotag);
t(over,capacidade_cache);

}

void Parte2(){
int nbits;
int larguraendereco;
int overhead;
int nconjunto;
int ntag;
int tamanho_indece;

printf("\nNumeros de bits do campo de indice do endereco: ");
scanf("%d",&nbits);
printf("\nLargura (em bytes) do endereco: ");
scanf("%d",&larguraendereco);
printf("\nOverhead (em bytes) da cache: ");
scanf("%d",&overhead);
system("clear");
printf("\n-------------------------------------------------------------------\n");
printf("-----------MODULO B-PARTE 2 TAMANHO DO BLOCO------------------------\n");
printf("-------------------------------------------------------------------\n\n");
printf("-----------------------------------------\n");
printf("------Dados Fornecidos pelo Usuario------\n");
printf("-----------------------------------------\n");
printf("Numeros de bits do campo de indice do endereco: %d\n",nbits);
printf("Largura (em bytes) do endereco: %d\n",larguraendereco);
printf("Overhead (em bytes) da cache: %d\n",overhead);
printf("\n");
printf("-----------------------------------------\n");
printf("-----------RESULTADOS--------------------\n");
printf("-----------------------------------------\n");
nconjunto=nconjuntos(nbits);
ntag=ntags(overhead,nconjunto);
tamanhobloco(larguraendereco,nbits,ntag);
tamanho_indece=campindice(nconjunto);
}

